<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopictreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topictrees', function (Blueprint $table) {
            $table->id();
            $table->string('title',64);
            $table->integer('status')->default('1');
            $table->unsignedBigInteger("topicparent_id")->nullable();
            $table->foreign('topicparent_id')->references('id')->on('topictrees')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topictrees');
    }
}
