<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('firstname',32);
            $table->string('lastname',32);
            $table->datetime('email_verified_at')->nullable();
            $table->string('landline',16)->nullable();
            $table->string('address');
            $table->string('mobile',16);
            $table->string('biography')->nullable();
            $table->integer('status')->default('1');
            $table->integer('gender');
            $table->Date('birthdate');
            $table->string('email',64)->unique();
            $table->string('secondemail',64)->unique()->nullable();
            $table->string('password');
            $table->unsignedBigInteger("country_id");
            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('cascade')->onDelete('cascade')->nullable();;
            $table->unsignedBigInteger("user_role_id")->nullable();
            $table->foreign('user_role_id')->references('id')->on('user_roles')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
