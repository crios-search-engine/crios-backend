<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRetrievedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retrieveds', function (Blueprint $table) {
            $table->id();
            $table->integer('rank');
            $table->unsignedBigInteger("document_id");
            $table->foreign('document_id')->references('id')->on('documents')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->unsignedBigInteger("query_id");
            $table->foreign('query_id')->references('id')->on('queries')->onUpdate('cascade')->onDelete('cascade')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('retrieved');
    }
}
