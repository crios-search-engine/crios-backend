<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profiletopic extends Model
{
    use HasFactory;
    protected $fillable = [
        'profile_id',
        'topictree_id'
    ];

    public function Profile()
    {
        return $this->belongsTo(Profile::class,'profile_id');
    }
    public function Topictree()
    {
        return $this->belongsTo(Topictree::class,'topictree_id');
    }
}
