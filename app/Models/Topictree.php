<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topictree extends Model
{
    use HasFactory;
    protected $fillable =[
        'title',
        'status',
        'topicparent_id'
    ];

    public function Doctopic()
    {
        return $this->hasMany(Doctopic::class,'topictree_id');
    }
    public function Profiletopic()
    {
        return $this->hasMany(Profiletopic::class,'topictree_id');
    }
    public function Parenttopics()
    {
        return $this->hasMany(Topictree::class,'topicparent_id');
    }
    public function Parenttopic()
    {
        return $this->belongsTo(Topictree::class,'topicparent_id');
    }
}
