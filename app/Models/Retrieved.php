<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Retrieved extends Model
{
    use HasFactory;
    protected $fillable =[
        'rank',
        'document_id',
        'query_id'
    ];

    public function document()
    {
        return $this->belongsTo(Document::class,'document_id');
    }
    public function queryy()
    {
        return $this->belongsTo(Query::class,'query_id');
    }
}
