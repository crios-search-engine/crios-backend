<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctopic extends Model
{
    use HasFactory;
    protected $fillable = [
        'document_id',
        'topictree_id'
    ];

    public function Document()
    {
        return $this->belongsTo(Document::class,'document_id');
    }
    public function Topictree()
    {
        return $this->belongsTo(Topictree::class,'topictree_id');
    }
}
