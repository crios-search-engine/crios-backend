<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;
    protected $fillable =[
        'title',
        'description',
        'user_id'
    ];

    public function Profiletopic()
    {
        return $this->hasMany(Profiletopic::class,'profile_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function queryy()
    {
        return $this->hasMany(Query::class,'profile_id');
    }
}
