<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EligibleUser extends Model
{
    use HasFactory;
    protected $fillable = [
        'email',
        'username'
    ];
}
