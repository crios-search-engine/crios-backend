<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;
    protected $fillable =[
        'name',
        'title',
        'path',
        'format',
        'description',
        'status',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    public function Doctopic()
    {
        return $this->hasMany(Doctopic::class,'document_id');
    }
    public function retrievedd()
    {
        return $this->hasMany(Retrieved::class,'document_id');
    }
}
