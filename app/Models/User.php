<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements CanResetPassword,MustVerifyEmail
{
    use Uuids;

    use HasFactory, Notifiable,HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'mobile',
        'landline',
        'address',
        'biography',
        'status',
        'gender',
        'birthdate',
        'email',
        'secondemail',
        'password',
        'country_id',
        'user_role_id',
        'google_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class,'country_id');
    }
    public function userRoles()
    {
        return $this->belongsTo(UserRoles::class,'user_role_id');
    }
    public function Profile()
    {
        return $this->hasMany(Profile::class,'user_id');
    }
    public function Document()
    {
        return $this->hasMany(Document::class,'user_id');
    }
    public function queryy()
    {
        return $this->hasMany(Query::class,'user_id');
    }
    public function TopicRequest()
    {
        return $this->hasMany(TopicRequest::class,'user_id');
    }
}
