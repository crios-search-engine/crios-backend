<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    use HasFactory;
    protected $fillable = [
        'title'
    ];
    public function user()
    {
        return $this->hasMany(User::class,'user_role_id');
    }
    public function rolePrivilege()
    {
        return $this->hasMany(RolePrivilege::class,'user_role_id');
    }
}
