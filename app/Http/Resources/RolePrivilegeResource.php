<?php

namespace App\Http\Resources;

use App\Http\Resources\UserRolesResource;
use App\Http\Resources\PrivilegeResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\UserRoles;
use App\Models\Privilege;

class RolePrivilegeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' =>$this->id,
            'user_role_id' => $this->user_role_id,
            'privilege_id' => $this->privilege_id,
            'title' =>(new UserRolesResource(UserRoles::findOrFail($this->user_role_id)))->title,
            'description' =>(new PrivilegeResource(Privilege::findOrFail($this->privilege_id)))->description,
            'code' =>(new PrivilegeResource(Privilege::findOrFail($this->privilege_id)))->code,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
