<?php

namespace App\Http\Resources;

use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Resources\Json\JsonResource;

class QueryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'title' =>$this->title,
            'user'=> new UserResourceCollection(User::select('*')->where('id', $this->user_id)->get()),
            'profile'=> new ProfileResourceCollection(Profile::select('*')->where('id', $this->profile_id)->get()),
            'created_at' =>$this->created_at,
            'updated_at' =>$this->updated_at
        ];
    }
}
