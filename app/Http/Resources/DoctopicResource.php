<?php

namespace App\Http\Resources;

use App\Models\Document;
use App\Models\Topictree;
use Illuminate\Http\Resources\Json\JsonResource;

class DoctopicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'document_id' => $this->document_id,
            'topictree_id' => $this->topictree_id,
            'topictree'=> new TopictreeResourceCollection(Topictree::select('*')->where('id', $this->topictree_id)->get()),
            'created_at' =>$this->created_at,
            'updated_at' =>$this->updated_at
        ];
    }
}
