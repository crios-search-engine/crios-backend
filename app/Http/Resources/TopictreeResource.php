<?php

namespace App\Http\Resources;

use App\Models\Topictree;
use Illuminate\Http\Resources\Json\JsonResource;

class TopicTreeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'title' =>$this->title,
            'status' =>$this->status,
            'subtopics'=> new TopictreeResourceCollection(Topictree::select('*')->where('topicparent_id', $this->id)->get()),
            'created_at' =>$this->created_at,
            'updated_at' =>$this->updated_at
        ];
    }
}
