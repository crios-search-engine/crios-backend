<?php

namespace App\Http\Resources;

use App\Models\Doctopic;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class DocumentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'name' =>$this->name,
            'title' =>$this->title,
            'path' =>$this->path,
            'format' =>$this->format,
            'description' =>$this->description,
            'status' =>$this->status,
            'user'=> new UserResourceCollection(User::select('*')->where('id', $this->user_id)->get()),
            'topics' => new DoctopicResourceCollection(Doctopic::select('*')->where('document_id', $this->id)->get()),
            'created_at' =>$this->created_at,
            'updated_at' =>$this->updated_at
        ];
    }
}
