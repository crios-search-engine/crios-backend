<?php

namespace App\Http\Resources;

use App\Http\Resources\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;


class TopicRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'description' => $this->description,
            'user_id' => $this->user_id,
            'user_firstname' =>(new UserResource(User::findOrFail($this->user_id)))->firstname,
            'user_lastname' =>(new UserResource(User::findOrFail($this->user_id)))->lastname,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];    }
}
