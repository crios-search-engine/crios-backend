<?php

namespace App\Http\Resources;

use App\Http\Resources\ProfileResource;
use App\Http\Resources\TopictreeResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Profile;
use App\Models\Topictree;

class ProfileTopicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'profile_id' => $this->profile_id,
            'topictree_id' => $this->topictree_id,
            'profiletitle' =>(new ProfileResource(Profile::findOrFail($this->profile_id)))->title,
            'description' =>(new ProfileResource(Profile::findOrFail($this->profile_id)))->description,
            'status' =>(new TopictreeResource(Topictree::findOrFail($this->topictree_id)))->status,
            'topictreetitle' =>(new TopictreeResource(Topictree::findOrFail($this->topictree_id)))->title,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
