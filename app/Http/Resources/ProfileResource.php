<?php

namespace App\Http\Resources;

use App\Models\User;
use App\Models\Profiletopic;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' =>$this->id,
            'title' =>$this->title,
            'description' =>$this->description,
            'user'=> new UserResourceCollection(User::select('*')->where('id', $this->user_id)->get()),
            'topics'=> new ProfileTopicResourceCollection(Profiletopic::select('*')->where('profile_id', $this->id)->get()),
            'created_at' =>$this->created_at,
            'updated_at' =>$this->updated_at
        ];
    }
}
