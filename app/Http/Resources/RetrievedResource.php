<?php

namespace App\Http\Resources;

use App\Models\Query;
use App\Models\Document;
use Illuminate\Http\Resources\Json\JsonResource;

class RetrievedResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'rank' => $this->rank,
            'document_id' => $this->document_id,
            'query_id' => $this->query_id,
            'document'=> new DocumentResourceCollection(Document::select('*')->where('id', $this->document_id)->get()),
            'query'=> new QueryResourceCollection(Query::select('*')->where('id', $this->query_id)->get()),
            'created_at' =>$this->created_at,
            'updated_at' =>$this->updated_at
        ];
    }
}
