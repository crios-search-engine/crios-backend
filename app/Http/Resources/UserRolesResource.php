<?php

namespace App\Http\Resources;

use App\Models\RolePrivilege;
use Illuminate\Http\Resources\Json\JsonResource;

class UserRolesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'title' =>$this->title,
            'privileges'=> new RolePrivilegeResourceCollection(RolePrivilege::select('*')->where('user_role_id', $this->id)->get()),
            'created_at' =>$this->created_at,
            'updated_at' =>$this->updated_at
        ];
    }
}
