<?php

namespace App\Http\Resources;

use App\Models\Country;
use App\Models\UserRoles;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>$this->id,
            'email' =>$this->email,
            'firstname' =>$this->firstname,
            'lastname' =>$this->lastname,
            'mobile' =>$this->mobile,
            'landline' =>$this->landline,
            'address' =>$this->address,
            'biography' =>$this->biography,
            'status' =>$this->status,
            'gender' =>$this->gender,
            'birthdate' =>$this->birthdate,
            'secondemail' =>$this->secondemail,
            'country' => (new CountryResource(Country::find($this->country_id))),
            'user_role' => (new UserRolesResource(UserRoles::find($this->user_role_id))),
            'created_at' =>$this->created_at,
            'updated_at' =>$this->updated_at
        ];
    }
}
