<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Profile;
use App\Models\Profiletopic;
use Illuminate\Http\Request;
use App\Http\Resources\ProfileResource;
use App\Http\Resources\ProfileTopicResource;
use App\Http\Resources\ProfileResourceCollection;
use App\Http\Resources\ProfileTopicResourceCollection;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ProfileResourceCollection(Profile::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'user_id' => 'required',
            'topictree'=>'required'
        ]);
        try {
            $profile = Profile::create($request->all());
            foreach($request->topictree as $value){

                Profiletopic::create([
                    "profile_id"=>$profile->id,
                    "topictree_id"=>$value["topictree_id"]
                ]);
            }
        } catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new ProfileResource($profile);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);
        return new ProfileResource($profile);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profile = Profile::find($id);
        $profile->update($request->all());
        $topicsassigned = Profiletopic::where('profile_id', $id)->get();
        $assigned = array();
        $requested = array();
        
        foreach($request->topictree as $value){
            $requested[] = (int)$value["topictree_id"];
        }

        foreach ($topicsassigned as $object){
            $assigned[] = $object->topictree_id ;
        }

        for ($i=0; $i < count($requested); $i++) { 
            if (!in_array($requested[$i], $assigned)) {
                Profiletopic::create([
                    "profile_id"=>$profile->id,
                    "topictree_id"=>$requested[$i]
                ]);
            }
        }
        
        for ($i=0; $i < count($assigned); $i++) { 
                if (!in_array($assigned[$i], $requested)) {
                    Profiletopic::where('profile_id', $id)
                    ->where('topictree_id',$assigned[$i])
                    ->delete();
            }
        }

        return new ProfileResource($profile);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profile::find($id)->delete();
        return response()->json('Profile deleted successfully');
    }

    public function GetProfileByUserID($user_id)
    {
        return new ProfileResourceCollection(Profile::where('user_id', $user_id)->get());
    }
}
