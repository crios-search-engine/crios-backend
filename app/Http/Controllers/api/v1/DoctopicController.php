<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Doctopic;
use Illuminate\Http\Request;
use App\Http\Resources\DoctopicResource;
use App\Http\Resources\DoctopicResourceCollection;

class DoctopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new DoctopicResourceCollection(Doctopic::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'document_id' => 'required',
            'topictree_id' => 'required'
        ]);
        try {
            $doctopic = Doctopic::create($request->all());
        } catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new  DoctopicResource($doctopic);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Doctopic  $doctopic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doctopic = Doctopic::find($id);
        return new  DoctopicResource($doctopic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Doctopic  $doctopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $doctopic = Doctopic::find($id);
        $doctopic->update($request->all());
        return new  DoctopicResource($doctopic);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Doctopic  $doctopic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Doctopic::find($id)->delete();
        return response()->json('Doctopic deleted successfully');
    }
}
