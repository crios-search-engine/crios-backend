<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Resources\CountryResource;
use App\Http\Resources\CountryResourceCollection;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index():CountryResourceCollection
    {
        return new CountryResourceCollection(Country::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'name' => 'required',
            'nationality' => 'required',
            'phonecode' => 'required'
        ]);
        try
        {
            $country = Country::create($request->all());
        }
        catch (\Exception $e){
            if($e)
            {
                return response()->json(['error' => 'this country already existed'], 200);
            }
            else
            {
                return response()->json(['error' => $e->getMessage()], 200);
            }
        }
        return new CountryResource($country);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show($id):CountryResource
    {
        $country = country::find($id);
        return new CountryResource($country);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return CountryResource
     */
    public function update(Request $request,$id):CountryResource
    {
        $country = country::find($id);
        $country->update($request->all());
        return new CountryResource($country);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Country::find($id)->delete();
        return response()->json('Country deleted successfully');
    }
}
