<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Resources\EligibleUserResourse;
use App\Http\Resources\EligibleUserResourseCollection;
use App\Models\EligibleUser;
use Illuminate\Http\Request;
class EligibleUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index():EligibleUserResourseCollection
    {
        return new EligibleUserResourseCollection(EligibleUser::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $eligibleuser =EligibleUser::create( $request->all());
        return new EligibleUserResourse($eligibleuser);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EligibleUser  $eligibleuser
     * @return \Illuminate\Http\Response
     */
    public function show(EligibleUser $eligibleuser):EligibleUserResourse
    {
        return new EligibleUserResourse($eligibleuser);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EligibleUser  $eligibleuser
     * @return \Illuminate\Http\Response
     */
    public function update( EligibleUser $eligibleuser,Request $request):EligibleUserResourse
    {
        $eligibleuser -> update($request->all());
        return new EligibleUserResourse($eligibleuser);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EligibleUser  $eligibleuser
     * @return \Illuminate\Http\Response
     */
    public function destroy(EligibleUser $eligibleuser)
    {
        $eligibleuser->delete();
        return response()->json("Eligible user deleted ", 203 );
    }
}
