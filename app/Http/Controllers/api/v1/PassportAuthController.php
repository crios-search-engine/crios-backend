<?php

namespace App\Http\Controllers\api\v1;

use App\Models\EligibleUser;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Events\Verified;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use App\Http\Resources\UserResource;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class PassportAuthController extends Controller
{
    function checkeligible($email)
    {
        try{
            $registerd = User::where('email',$email) -> first();
            if($registerd){
                return response()->json(["registered"=>true], 202);
            }
            else {
                $eligible = EligibleUser::where('email',$email) -> first();
                return response()->json([ "registered"=>$registerd? true : false, "eligible"=>$eligible? true : false ], 202);
            }
        }
        catch (\Exception $e){
            return false;
        }
    }
    // to check people eligibility
    function checkIfEligible($email)
    {
        try{
            $bull = EligibleUser::where('email',$email) -> first();
            if($bull){
                return true;
            }
            else{
                return false;
            }
        }
        catch (\Exception $e){
            return false;
        }
    }
    //register a user
    public function register(Request $request)
    {
        $this->validate($request, [
                'firstname' => 'required:',
                'email' => 'required|email',
                'password' => 'required',
        ]);

        if(!$this->checkIfEligible($request->email)){
            return response()->json("this account is not in the eligible users ", 401);
        }
        try {
            $request['password'] = bcrypt($request->password);
            $user = User::create($request->all());
            event(new Registered($user));
        }
        catch (\Exception $e){
            if($e->errorInfo[1]=='1062'){
                return response()->json(['error' => 'this account already existed'], 200);
            }
            else {
                return response()->json(['error' => $e->getMessage()], 200);
            }
        }
        $token = $user->createToken('LaravelAuthApp',['P01'])->accessToken;
        return response()->json(['token' => $token,'data'=>new UserResource($user)], 200);
    }

    public function __invoke(Request $request): RedirectResponse
    {
        $user = User::find($request->route('id'));

        if ($user->hasVerifiedEmail()) {
            return redirect(env('FRONT_URL') . '/verified-account');
        }

        if ($user->markEmailAsVerified()) {
            event(new Verified($user));
        }

        return redirect(env('FRONT_URL') . '/verified-account');
    }
//    change password
    function changePassword(Request $request)
    {
        $input = $request->all();
        $userid = Auth::guard('api')->user()->id;
        $rules = array(
            'old_password' => 'required',
            'new_password' => 'required|min:6',
            'confirm_password' => 'required|same:new_password',
        );
        $validator = Validator::make($input, $rules);
        if ($validator->fails()) {
            $arr = array("message" => $validator->errors()->first(), "data" => array());
        } else {
            try {
                if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                    $arr = array("message" => "Check your old password.", "data" => array());
                    return response()->json($arr, 400);
                } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                    $arr = array("message" => "Please enter a password which is not similar then current password.", "data" => array());
                    return response()->json($arr, 400);
                } else {
                    User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                    $arr = array("message" => "Password updated successfully.", "data" => new UserResource(User::find(auth()->user()->id)));
                }
            } catch (\Exception $ex) {
                if (isset($ex->errorInfo[2])) {
                    $msg = $ex->errorInfo[2];
                } else {
                    $msg = $ex->getMessage();
                }
                $arr = array( "message" => $msg, "data" => array());
                return response()->json($arr, 400);
            }
        }
        return response()->json($arr, 200);
    }
    /**
     * Login
     */
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
            return response()->json(['token' => $token,'data'=>new UserResource(User::find(auth()->user()->id))], 200);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
    function forgetOPassword (Request $request) {
        $request->validate(['email' => 'required|email']);

        $status = Password::sendResetLink(
            $request->only('email')
        );

        response()->json( $status === Password::RESET_LINK_SENT
            ? back()->with(['status' => __($status)])
            : back()->withErrors(['email' => __($status)]) . "email sent successfully", 200);

    }
    function resetPassword(Request $request) {
        $request->validate([
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);

        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user, $password) use ($request) {
                $user->forceFill([
                    'password' => Hash::make($password)
                ])->setRememberToken(Str::random(60));

                $user->save();

                event(new PasswordReset($user));
            }
        );

        return response()->json("Password changed successfully ", 200);
    }
    function openResetPasswordView (Request $request, $token) {
        return redirect(env('FRONT_URL') . '/reset-password?token='.($token . '&&email=' . $request['email']));

    }
    function googleSignIn (Request $request) {
        $User = User::where('email',$request->email) -> first();
        if($User){
            $data = [
                'email' => $request->email,
                'password' => $request->password,
            ];
            if (auth()->attempt($data)) {
                $token = auth()->user()->createToken('LaravelAuthApp')->accessToken;
                return response()->json(['token' => $token,'data'=>new UserResource($User)], 200);
            } else {
                return response()->json(['error' => 'Unauthorised'], 401);
            }
        }
        else{
            if(!$this->checkIfEligible($request->email)){
                return response()->json("this email is not in the eligible users ", 401);
            }
            try{
                $Newuser = User::create([
                    'email' => $request->email,
                    'google_id' => $request->google_id,
                    'password' => bcrypt($request->password),
                    'firstname' => $request->firstname,
                    'lastname' => $request->lastname,
                    'address' =>$request->address,
                    'mobile' => $request->mobile,
                    'gender' => $request->gender,
                    'birthdate' => $request->birthdate,
                    'country_id' => $request->country_id

                ]);
                event(new Registered($Newuser));
                $token = $Newuser->createToken('LaravelAuthApp',['P01'])->accessToken;
            }
            catch (\Exception $e){
                if($e->errorInfo[1]=='1062'){
                    return response()->json(['error' => 'this account already existed'], 200);
                }
                else {
                    return response()->json(['error' => $e->getMessage()], 200);
                }
            }
            return response()->json(['token' => $token,'data'=>new UserResource($Newuser)], 200);

        }
    }
}
