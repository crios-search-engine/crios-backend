<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Document;
use App\Models\Doctopic;
use App\Models\file;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\DocumentResource;
use App\Http\Resources\DocumentResourceCollection;
use PhpParser\Node\Expr\Cast\Object_;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status = request('status');
        if ($status != null){

            return new DocumentResourceCollection(Document::where('status', $status)->get());
        }
        return new DocumentResourceCollection(Document::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return DocumentResource
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'file' => 'required|mimes:doc,docx,pdf,txt,csv|max:2048',
        ]);

        if($validator->fails()) {

            return response()->json(['error'=>$validator->errors()], 401);
        }

        $request->validate([
            'name' => 'required',
            'title' => 'required',
            'description' => 'required',
            'status',
            'user_id' => 'required'
        ]);
        try {
            if ($file = $request->file('file')) {
                $path = $file->store('public/files');
                $name = $file->getClientOriginalName();
                $format = $file->getClientOriginalExtension();
                //store your file into directory and db
                $documentInof = $request->all();
                $documentInof['path']= $path;
                $documentInof['format']= $format;
                $documentInof['topictree'] = (Array)($documentInof['topictree']);
                $topicArray =  (Array)($documentInof['topictree']);
                $document = Document::create($documentInof);
            }
            foreach($documentInof['topictree'] as $value){
                Doctopic::create([
                    "document_id"=>$document->id,
                    "topictree_id"=>$value
                ]);
            }

        } catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new  DocumentResource($document);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $document = Document::find($id);
        return new DocumentResource($document);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $document = Document::find($id);
        $document->update($request->all());
        return new DocumentResource($document);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Document  $document
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Document::find($id)->delete();
        return response()->json('Document deleted successfully');
    }

    public function GetDocumentByUserID($user_id)
    {
        return new DocumentResourceCollection(Document::where('user_id', $user_id)->get());
    }

    
   

    public function ValidateDocuments(Request $request)
    {
        foreach($request->documents as $value){
            $id=$value["document_id"];
            $document = Document::find($id);
            if($document->status =='1') {
                $document->status = '2';
                $document->save();
            }
        }
        return response()->json('Documents are validated successfully');
    }

    public function IndexDocuments(Request $request)
    {
        foreach($request->documents as $value){
            $id=$value["document_id"];
            $document = Document::find($id);
            if($document->status =='2') {
                $document->status = '3';
                $document->save();
            }
        }

        $document5 = Document::select('*')->where('status', 5)->get();;
        foreach($document5 as $document5){
            $document5->status = '0';
            $document5->save();
        }

        $document4 = Document::select('*')->where('status', 4)->get();;
        foreach($document4 as $document4){
            $id = $document4->id;
            Document::find($id)->delete();
            $document4->save();
        }

        return response()->json('Documents with status 2 are indexed successfully,Documents with status 4 are deleted successfully,Documents with status 5 are hidden successfully');
    }

    public function HideDocument($id)
    {
        $document = Document::find($id);
        if($document->status =='1' || $document->status =='2') {
            $document->status = '0';
            $document->save();
            return response()->json('Document hidden successfully');
        }else if ($document->status =='3' || $document->status =='4') {
            $document->status = '5';
            $document->save();
            return response()->json('Document will be hidden soon');
        }
        
    }

    public function DeleteDocument($id)
    {
        $document = Document::find($id);
        if($document->status =='0' || $document->status =='1' || $document->status =='2') {
            $id = $document->status;
            Document::find($id)->delete();
            $document->save();
            return response()->json('Document deleted successfully');
        }else if ($document->status =='3' || $document->status =='5') {
            $document->status = '4';
            $document->save();
            return response()->json('Document will be deleted soon');
        }
        
    }
}
