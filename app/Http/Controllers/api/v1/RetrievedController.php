<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Retrieved;
use Illuminate\Http\Request;
use App\Http\Resources\RetrievedResource;
use App\Http\Resources\RetrievedResourceCollection;

class RetrievedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new RetrievedResourceCollection(Retrieved::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        $request->validate([
            'rank' => 'required',
            'document_id' => 'required',
            'query_id' => 'required'
        ]);
        try {
        $retrieved = Retrieved::create($request->all());
        
        } catch (\Exception $e){

            return response()->json(['error' => $e->getMessage()], 200);
        }
        return new RetrievedResource($retrieved);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $retrieved = Retrieved::find($id);
        return new RetrievedResource($retrieved);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $retrieved = Retrieved::find($id);
        $retrieved->update($request->all());
        return new RetrievedResource($retrieved);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Retrieved::find($id)->delete();
        return response()->json('Retrieved deleted successfully');
    }

    public function GetRetrievedByDocumentID($document_id)
    {
        return new RetrievedResourceCollection(Retrieved::where('document_id', $document_id)->get());
    }

    public function GetRetrievedByQueryID($query_id)
    {
        return new RetrievedResourceCollection(Retrieved::where('query_id', $query_id)->get());
    }
}
