<?php

namespace App\Http\Controllers\api\v1;


use App\Models\UserRoles;
use App\Models\RolePrivilege;
use Illuminate\Http\Request;
use App\Http\Resources\UserRolesResource;
use App\Http\Resources\UserRolesResourceCollection;

class UserRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new UserRolesResourceCollection(UserRoles::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'privilege'=>'required'
        ]);
        try {
            $userRoles = UserRoles::create($request->all());
            foreach($request->privilege as $value){

                RolePrivilege::create([
                    "user_role_id"=>$userRoles->id,
                    "privilege_id"=>$value["privilege_id"]
                ]);
            }
        } catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new UserRolesResource($userRoles);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserRoles  $userRoles
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userRoles = UserRoles::find($id);
        return new UserRolesResource($userRoles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserRoles  $userRoles
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userRoles = UserRoles::find($id);
        $userRoles->update($request->all());
        return new UserRolesResource($userRoles);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserRoles  $userRoles
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        UserRoles::find($id)->delete();
        return response()->json('UserRole deleted successfully');
    }
}
