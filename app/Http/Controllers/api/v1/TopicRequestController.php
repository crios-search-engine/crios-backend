<?php

namespace App\Http\Controllers\api\v1;

use App\Models\TopicRequest;
use Illuminate\Http\Request;
use App\Http\Resources\TopicRequestResource;
use App\Http\Resources\TopicRequestResourceCollection;

class TopicRequestController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new TopicRequestResourceCollection(TopicRequest::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'user_id' => 'required',
        ]);
        try {
            $topicrequest = TopicRequest::create($request->all());

        } catch (\Exception $e){
            
                return response()->json(['error' => $e->getMessage()], 200);
        }

        return new TopicRequestResource($topicrequest);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topicrequest = TopicRequest::find($id);
        return new TopicRequestResource($topicrequest);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Country  $country
     * @return CountryResource
     */
    public function update(Request $request,$id)
    {
        $topicrequest = TopicRequest::find($id);
        $topicrequest->update($request->all());
        return new TopicRequestResource($topicrequest);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        TopicRequest::find($id)->delete();
        return response()->json('Topic Request deleted successfully');
    }
}
