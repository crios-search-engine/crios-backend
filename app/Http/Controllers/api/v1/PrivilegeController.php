<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Privilege;
use Illuminate\Http\Request;
use App\Http\Resources\PrivilegeResource;
use App\Http\Resources\PrivilegeResourceCollection;

class PrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new PrivilegeResourceCollection(Privilege::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required',
            'code' => 'required'
        ]);
        try {
            $privilege = Privilege::create($request->all());
        } catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new PrivilegeResource($privilege);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $privilege = Privilege::find($id);
        return new PrivilegeResource($privilege);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $privilege = Privilege::find($id);
        $privilege->update($request->all());
        return new PrivilegeResource($privilege);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Privilege  $privilege
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Privilege::find($id)->delete();
        return response()->json('Privilege deleted successfully');
    }
}
