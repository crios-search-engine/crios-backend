<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Profiletopic;
use Illuminate\Http\Request;
use App\Http\Resources\ProfileTopicResource;
use App\Http\Resources\ProfileTopicResourceCollection;

class ProfiletopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ProfiletopicResourceCollection(Profiletopic::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'profile_id' => 'required',
            'topictree_id' => 'required'
        ]);
        try {
            $profiletopic = Profiletopic::create($request->all());

        } catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new  ProfiletopicResource($profiletopic);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profiletopic  $profiletopic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profiletopic = Profiletopic::find($id);
        return new  ProfiletopicResource($profiletopic);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profiletopic  $profiletopic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profiletopic = Profiletopic::find($id);
        $profiletopic->update($request->all());
        return new  ProfiletopicResource($profiletopic);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profiletopic  $profiletopic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Profiletopic::find($id)->delete();
        return response()->json('Profiletopic deleted successfully');
    }
}
