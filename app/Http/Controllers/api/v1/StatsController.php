<?php

namespace App\Http\Controllers\api\v1;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Document;
use App\Models\Topictree;

class StatsController extends Controller
{    
    public function GetStats()
    {
        $data          = array();
        $userCount = User::count(); 
        $documentCount = Document::count(); 
        $topicCount = Topictree::count(); 

        $data[]       = [
            'Users' => $userCount,
            'Documents' => $documentCount,
            'Topics' => $topicCount
        ];

        return response()->json($data);
    }
}
