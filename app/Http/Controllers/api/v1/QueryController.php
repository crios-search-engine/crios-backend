<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Query;
use Illuminate\Http\Request;
use App\Http\Resources\QueryResource;
use App\Http\Resources\QueryResourceCollection;

class QueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new QueryResourceCollection(Query::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'user_id' => 'required',
            'profile_id' => 'required'
        ]);
        try {
            $query = Query::create($request->all());

        } catch (\Exception $e){
            
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new QueryResource($query);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $query = Query::find($id);
        return new QueryResource($query);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $query = Query::find($id);
        $query->update($request->all());
        return new QueryResource($query);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Query::find($id)->delete();
        return response()->json('Query deleted successfully');
    }

    public function GetQueryByUserID($user_id)
    {
        return new QueryResourceCollection(Query::where('user_id', $user_id)->get());
    }

    public function GetQueryByProfileID($profile_id)
    {
        return new QueryResourceCollection(Query::where('profile_id', $profile_id)->get());
    }
}
