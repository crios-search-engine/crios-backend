<?php

namespace App\Http\Controllers\api\v1;

use App\Models\Topictree;
use Illuminate\Http\Request;
use App\Http\Resources\TopictreeResource;
use App\Http\Resources\TopictreeResourceCollection;

class TopictreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new TopictreeResourceCollection(Topictree::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'status',
            'topicparent_id'
            
        ]);
        try {
            $topictree = Topictree::create($request->all());
        } catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new TopictreeResource($topictree);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topictree  $topictree
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topictree = Topictree::find($id);
        return new TopictreeResource($topictree);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Topictree  $topictree
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $topictree = Topictree::find($id);
        $topictree->update($request->all());
        return new TopictreeResource($topictree);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Topictree  $topictree
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Topictree::find($id)->delete();
        return response()->json('Topictree deleted successfully');
    }
}
