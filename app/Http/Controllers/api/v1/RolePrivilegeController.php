<?php

namespace App\Http\Controllers\api\v1;

use App\Models\RolePrivilege;
use Illuminate\Http\Request;
use App\Http\Resources\RolePrivilegeResource;
use App\Http\Resources\RolePrivilegeResourceCollection;

class RolePrivilegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new RolePrivilegeResourceCollection(RolePrivilege::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_role_id' => 'required',
            'privilege_id' => 'required'
        ]);
        try {
            $rolePrivilege = RolePrivilege::create($request->all());

        } catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()], 200);
        }
        return new  RolePrivilegeResource($rolePrivilege);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RolePrivilege  $rolePrivilege
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rolePrivilege = RolePrivilege::find($id);
        return new  RolePrivilegeResource($rolePrivilege);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RolePrivilege  $rolePrivilege
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rolePrivilege = RolePrivilege::find($id);
        $rolePrivilege->update($request->all());
        return new  RolePrivilegeResource($rolePrivilege);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RolePrivilege  $rolePrivilege
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RolePrivilege::find($id)->delete();
        return response()->json('RolePrivilege deleted successfully');
    }
}
