<?php

use App\Http\Controllers\api\v1\PassportAuthController;
use App\Http\Controllers\api\v1\TopicRequestController;
use App\Http\Controllers\api\v1\StatsController;
use App\Http\Controllers\api\v1\PostController;
use App\Http\Controllers\api\v1\UserController;
use App\Http\Controllers\api\v1\CountryController;
use App\Http\Controllers\api\v1\PrivilegeController;
use App\Http\Controllers\api\v1\UserRolesController;
use App\Http\Controllers\api\v1\RolePrivilegeController;
use App\Http\Controllers\api\v1\TopictreeController;
use App\Http\Controllers\api\v1\ProfileController;
use App\Http\Controllers\api\v1\QueryController;
use App\Http\Controllers\api\v1\RetrievedController;
use App\Http\Controllers\api\v1\ProfiletopicController;
use App\Http\Controllers\api\v1\DoctopicController;
use App\Http\Controllers\api\v1\DocumentController;
use \App\Http\Controllers\api\v1\EligibleUserController;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

//resources
Route::apiResource('/country', CountryController::class);
Route::apiResource('/privilege', PrivilegeController::class);
Route::apiResource('/userroles', UserRolesController::class);
Route::apiResource('/roleprivilege', RolePrivilegeController::class);
Route::apiResource('/topictree', TopictreeController::class);
Route::apiResource('/profile', ProfileController::class);
Route::apiResource('/profiletopic', ProfiletopicController::class);
Route::apiResource('/query', QueryController::class);
Route::apiResource('/retrieved', RetrievedController::class);
Route::apiResource('/doctopic', DoctopicController::class);
Route::apiResource('/document', DocumentController::class);
Route::apiResource('/user', UserController::class);
Route::apiResource('/eligibleuser', EligibleUserController::class);
Route::apiResource('/topicrequest', TopicRequestController::class);

//Custom apis
Route::post('/validatedocuments',[DocumentController::class, 'ValidateDocuments']);
Route::post('/indexdocuments',[DocumentController::class, 'IndexDocuments']);
Route::get('/hidedocument/{id}',[DocumentController::class, 'HideDocument']);
Route::get('/deletedocument/{id}',[DocumentController::class, 'DeleteDocument']);
Route::get('/user/{id}/document',[DocumentController::class, 'GetDocumentByUserID']);
Route::get('/user/{id}/profile',[ProfileController::class, 'GetProfileByUserID']);
Route::get('/user/{id}/query',[QueryController::class, 'GetQueryByUserID']);
Route::get('/profile/{id}/query',[QueryController::class, 'GetQueryByProfileID']);
Route::get('/document/{id}/retrieved',[RetrievedController::class, 'GetRetrievedByDocumentID']);
Route::get('/query/{id}/retrieved',[RetrievedController::class, 'GetRetrievedByQueryID']);
Route::get('/stats',[StatsController::class, 'GetStats']);

//users routes
Route::post('signup', [PassportAuthController::class, 'register']);
Route::post('signin', [PassportAuthController::class, 'login']);

Route::get('/email/verify/{id}/{hash}', [PassportAuthController::class, '__invoke'])
    ->middleware(['signed', 'throttle:6,1'])
    ->name('verification.verify');

// Resend link to verify email
Route::post('/email/verify/resend', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();
    return back()->with('message', 'Verification link sent!');
})->middleware(['auth:api', 'throttle:6,1'])->name('verification.send');
Route::get('/check-user/{email}', [PassportAuthController::class, 'checkeligible']);
//Route::get('/email/verify/{id}/{hash}', [PassportAuthController::class,'verifyMail']
//)->middleware(['auth:api'])->name('verification.verify');
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');
//change password by old password
Route::post('/change-password',[PassportAuthController::class,'changePassword']
)->middleware('auth:api')->name('password.change');
Route::post('/forgot-password',[PassportAuthController::class,'forgetOPassword']
)->middleware('guest')->name('password.email');
Route::get('/reset-password/{token}', [PassportAuthController::class,'openResetPasswordView']
)->middleware('guest')->name('password.reset');
Route::post('/reset-password', [PassportAuthController::class,'resetPassword']
)->middleware('guest')->name('password.update');
Route::post('/auth',[PassportAuthController::class,'googleSignIn'] );

//for testing purposes
Route::middleware(['auth:api','scopes:P01'])->group(function () {
    Route::resource('posts', PostController::class);
});
